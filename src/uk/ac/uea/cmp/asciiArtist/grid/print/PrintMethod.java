package uk.ac.uea.cmp.asciiArtist.grid.print;

import java.util.HashMap;

import uk.ac.uea.cmp.asciiArtist.grid.Grid;

public abstract class PrintMethod {
	abstract public void Print(Grid g) throws PrintException;
	
	protected HashMap<String,String> options = new HashMap<String,String>();
	
	public void setOption(String option, String value)
	{
		options.put(option, value);
	}
	
	public String getOption(String option)
	{
		return options.get(option);
	}
	
	
}
