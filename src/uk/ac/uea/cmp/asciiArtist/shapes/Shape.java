package uk.ac.uea.cmp.asciiArtist.shapes;

import uk.ac.uea.cmp.asciiArtist.grid.*;

public abstract class Shape implements GridDraw {
	
	protected int R;
	protected Grid g;
	
	public Grid getGrid()
	{
		return g;
	}
	
	public void Draw(Grid canvas, int xpos, int ypos)
	{
		canvas.Overlay(g, xpos, ypos);
	}
	
	public int getX()
	{
		return g.getX();
	}
	
	public int getY()
	{
		return g.getY();
	}
}
