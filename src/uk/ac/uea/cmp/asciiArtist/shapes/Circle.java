package uk.ac.uea.cmp.asciiArtist.shapes;

import uk.ac.uea.cmp.asciiArtist.grid.Grid;

public class Circle extends Shape {

	public Circle(int r)
	{
		R = r;
		int x = (r*2)+2;
		int y = x;
		
		g = new Grid(x,y);
		
		int cx = r+1;
		int cy = cx;
		
		for(double rad = 0; rad < Math.PI*2; rad+=((Math.PI*2)/360))
		{
			int xp = (int)(Math.cos(rad)*r)+cx;
			int yp = (int)(Math.sin(rad)*r)+cy;
			try
			{
				g.Set(xp,yp,1);
			}
			catch(Exception e)
			{
				//
			}
		}
	}
	
}
