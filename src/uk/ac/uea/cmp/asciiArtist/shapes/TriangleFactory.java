package uk.ac.uea.cmp.asciiArtist.shapes;

public class TriangleFactory extends ShapeFactory {

	@Override
	public Shape Create(int r) {
		return new Triangle(r);
	}

	static private ShapeFactory instance = null;
	static public ShapeFactory getInstance()
	{
		if (instance == null)
			return new TriangleFactory();
		return instance;
	}
	
	private TriangleFactory() { }


}
